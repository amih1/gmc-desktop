'use strict';

module.exports = function() {

    $.gulp.task('scripts:lib', function () {
        return $.gulp.src(['src/vendor/slick/slick.min.js'])
            .pipe($.gp.plumber())                              // отлавливаю ошибки 
            //.pipe($.gp.concat('all.libs.js'))                // конкатенация js 
            //.pipe($.gp.uglify())                             // минификация js
            //.pipe($.gp.rename('all.libs.min.js'))            // переименовываю all.libs.min.js
            .pipe($.gulp.dest('dest/js/'))
            .pipe($.bs.reload({                                // позволяет обновить страницу только когда таски выполняться
                stream:true                                    // не перемещаем страницу при обновалении
            }));
    });

    $.gulp.task('scripts', function () {
        return $.gulp.src('src/static/js/*.js')
            .pipe($.gp.plumber())
            .pipe($.gp.concat('main.js'))
            //.pipe($.gp.uglify())
            // .pipe($.gp.rename('all.common.min.js'))
            .pipe($.gulp.dest('dest/js/'))
            .pipe($.bs.reload({
                stream: true
            }));
    });
};
