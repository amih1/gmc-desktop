'use strict';

module.exports = function() {
    $.gulp.task('sass', function () {
        return $.gulp.src('src/static/sass/*.sass')
            .pipe($.gp.sourcemaps.init())
            .pipe($.gp.sass())
            .pipe($.gp.sourcemaps.write())
            .pipe($.gp.plumber())
            .pipe($.gulp.dest('./dest/css/'))
            .pipe($.bs.reload({
                stream: true
            }));
    });


    $.gulp.task('libs_css', function () {
        return $.gulp.src([
            'src/vendor/slick/slick.css',
            'src/vendor/slick/slick-theme.css'
            ])
            .pipe($.gp.plumber())                             // отлаливаю ошибки и не даю вылететь gulp
            //.pipe($.gp.concatCss('libs.min.css'))           // конктинирую css
            .pipe($.gp.csso())                              //минификация
            .pipe($.gulp.dest('dest/css/'))
            .pipe($.bs.reload({
                stream: true
            }));
    });

    $.gulp.task('fontSlick', function () {
        return $.gulp.src([
            'src/vendor/slick/fonts/*',
            'src/static/fonts/*'])
            .pipe($.gp.plumber())                             // отлаливаю ошибки и не даю вылететь gulp
            .pipe($.gulp.dest('dest/webfonts'));
    });
};

